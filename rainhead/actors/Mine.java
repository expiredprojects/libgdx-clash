package o.rainhead.actors;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import o.rainhead.core.GameData;
import o.rainhead.interfaces.Updateable;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 9/2/13
 * Time: 1:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class Mine extends Sprite implements Updateable {
    private final int pivotX;
    private final int pivotY;
    private int dx;
    private int dy;

    public Mine(TextureRegion region, int x, int y, int dx, int dy) {
        super(region);
        this.pivotX = (int) (this.getWidth() / 2);
        this.pivotY = (int) (this.getHeight() / 2);
        setPosition(x - pivotX, y - pivotY);
        this.dx = dx;
        this.dy = dy;
    }

    @Override
    public void update() {
        modX();
        modY();
    }

    private void modX() {
        setX(getX() - dx);
        dx = (int) checkBounds(getX(), 0 - pivotX, GameData.SCREEN_WIDTH - pivotX, dx);
    }

    private void modY() {
        setY(getY() - dy);
        dy = (int) checkBounds(getY(), 0 - pivotY, GameData.SCREEN_HEIGHT - pivotY, dy);
    }

    private float checkBounds(float value, float left, float right, float delta) {
        if (value - delta <= left){
            return -delta;
        }
        else if (value - delta >= right){
            return  -delta;
        }
        else {
            return delta;
        }
    }
}
