package o.rainhead.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import o.rainhead.interfaces.Renderable;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 8/31/13
 * Time: 12:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class Background extends Sprite implements Renderable {

    public Background(TextureRegion region) {
        super(region);
    }

    @Override
    public void render(SpriteBatch batch) {
        int sceneWidth = Gdx.graphics.getWidth();
        int sceneHeight = Gdx.graphics.getHeight();
        int regionWidth = this.getRegionWidth();
        int regionHeight = this.getRegionWidth();

        for (int i = 0; i < sceneWidth; i = i + regionWidth) {
            for (int j = 0; j < sceneHeight; j = j + regionHeight) {
                this.setPosition(i, j);
                this.draw(batch);
            }
        }
    }
}
