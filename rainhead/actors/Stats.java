package o.rainhead.actors;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import o.rainhead.core.GameData;
import o.rainhead.interfaces.Renderable;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 9/2/13
 * Time: 4:30 PM
 * To change this template use File | Settings | File Templates.
 */
public class Stats implements Renderable {
    private final BitmapFont bitmapFont;
    private Date date;
    private long time;
    private long old;
    private int frames = 0;
    private float tp60 = 0;

    public Stats() {
        bitmapFont = new BitmapFont();
        bitmapFont.setScale(1, -1);

        date = new Date();
        old = date.getTime();
    }

    @Override
    public void render(SpriteBatch batch) {
        enterFrame();
        bitmapFont.draw(batch, "tp60: " + tp60, 5, 5);
        bitmapFont.draw(batch, "obj: " + GameData.OBJECTS, 5, 20);
    }

    private void enterFrame() {
        if (++frames == 60) {
            date = new Date();
            time = date.getTime();
            tp60 = (float) (time - old) / 1000;
            old = time;
            frames = 0;
        }
    }
}
