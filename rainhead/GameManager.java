package o.rainhead;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import o.rainhead.actors.Background;
import o.rainhead.actors.Stats;
import o.rainhead.core.DisplayManager;
import o.rainhead.input.GameInput;

public class GameManager implements ApplicationListener {
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private Texture texture;
    private Background background;
    private DisplayManager displayManager;
    private Stats stats;

    @Override
	public void create() {
        camera = new OrthographicCamera();
        updateCamera();

        batch = new SpriteBatch();
        texture = new Texture(Gdx.files.internal("data/clash.png"));
        texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);

        initScreen();

        GameInput gameInput = new GameInput(displayManager);
        Gdx.input.setInputProcessor(gameInput);
    }

    private void updateCamera() {
        int w = Gdx.graphics.getWidth();
        int h = Gdx.graphics.getHeight();
        camera.setToOrtho(true, w, h);
    }

    private void initScreen() {
        TextureRegion bgRegion = new TextureRegion(texture, 0, 0, 100, 100);
        background = new Background(bgRegion);

        TextureRegion mineRegion = new TextureRegion(texture, 102, 60, 40, 40);
        displayManager = new DisplayManager(mineRegion);

        stats = new Stats();
    }

    @Override
	public void dispose() {
        texture.dispose();
		batch.dispose();
	}

	@Override
	public void render() {
        clear();
        draw();
	}

    private void clear() {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
    }

    private void draw() {
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        background.render(batch);
        displayManager.render(batch);
        stats.render(batch);
        batch.end();
    }

    @Override
	public void resize(int width, int height) {
        updateCamera();
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}
