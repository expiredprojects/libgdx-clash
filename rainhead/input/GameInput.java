package o.rainhead.input;

import com.badlogic.gdx.Input;
import o.rainhead.core.DisplayManager;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 9/2/13
 * Time: 3:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class GameInput extends AbstractProcessor {
    private final DisplayManager displayManager;

    public GameInput(DisplayManager displayManager) {
        this.displayManager = displayManager;
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.LEFT) {
            displayManager.removeElements(10);
        }
        else if (keycode == Input.Keys.UP) {
            displayManager.addElements(100);
        }
        else if (keycode == Input.Keys.RIGHT) {
            displayManager.addElements(10);
        }
        else if (keycode == Input.Keys.DOWN) {
            displayManager.removeElements(100);
        }
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
