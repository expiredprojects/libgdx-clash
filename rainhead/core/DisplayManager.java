package o.rainhead.core;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import o.rainhead.actors.Mine;
import o.rainhead.interfaces.Displayable;
import o.rainhead.interfaces.Renderable;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 9/2/13
 * Time: 1:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class DisplayManager implements Renderable, Displayable {
    private final TextureRegion region;
    private final ArrayList<Mine> mineArrayList;

    public DisplayManager(TextureRegion region) {
        this.region = region;
        mineArrayList = new ArrayList<Mine>();
    }

    @Override
    public void render(SpriteBatch batch) {
        for (Mine mine : mineArrayList){
            mine.update();
            mine.draw(batch);
        }
    }

    private int randomOne() {
        return -1 + 2 * (int) (Math.random() * 2);
    }

    @Override
    public void addElements(int number) {
        int x, y, dx, dy;
        for (int i = 0; i < number; i++){
            x = (int) (Math.random() * GameData.SCREEN_WIDTH);
            y = (int) (Math.random() * GameData.SCREEN_HEIGHT);
            dx = randomOne();
            dy = randomOne();
            mineArrayList.add(new Mine(region, x, y, dx, dy));
        }

        GameData.OBJECTS = mineArrayList.size();
    }

    @Override
    public void removeElements(int number) {
        number = mineArrayList.size() - 1 - number;
        for (int i = mineArrayList.size() - 1; i > number && i > -1; i--){
            mineArrayList.remove(i);
        }

        GameData.OBJECTS = mineArrayList.size();
    }
}
