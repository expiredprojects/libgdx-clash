package o.rainhead.interfaces;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 9/2/13
 * Time: 10:03 AM
 * To change this template use File | Settings | File Templates.
 */
public interface Renderable {
    public void render(SpriteBatch batch);
}
