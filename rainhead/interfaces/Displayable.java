package o.rainhead.interfaces;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 9/2/13
 * Time: 2:24 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Displayable {
    public void addElements(int number);
    public void removeElements(int number);
}
